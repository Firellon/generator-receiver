require("dotenv").config();

var argv = require("minimist")(process.argv.slice(2));
var randomstring = require("randomstring");
var Promise = require('bluebird')

var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var client = redis.createClient();
var subscriber = client.duplicate()


var id = Date.now();
var role = "";
var free = 1;
var loopTime = 500; // how much time passes between generation/message fetching, set 1 for test
var messageProcessingTime = 500; // how much time worker needs for one message, set 1 for test
var lifeTime = 1000; // How much time needed for generator/receiver id data to expire
var maxBlockingTime = 1; // how many seconds redis will wait until unblocking the line for pop
var errorProbability = 0.05; // value in range from 0 to 1
var messagesMax = 1000000; // 0 for no limit

var log = (...message) => {
  if (process.env.NODE_ENV === "development") {
    console.log(...message);
  }
};

var getAllWorkers = () => {
  var scanInd = 0
  var workers = []
  return Promise.resolve().then(function scanLoop() {
    return client.scanAsync(scanInd, "MATCH", "worker:*").then((result) => {
      scanInd = result[0]
      workers = workers.concat(result[1])
      if (scanInd == 0) {
        // Scan is finished
        return workers.sort()
      } else {
        // Keep scanning
        return scanLoop()
      }
    })
  })
  .catch((err) => {
    console.error("getAllWorkers@err:", err);
  });
}

var init = () => {
  var myKey = "worker:"+id
  return client.setAsync(myKey, 1, "PX", lifeTime)
  .then(() => {
    return getAllWorkers()
  })
  .then((workers) => {
    // log("init@workers:", workers)
    if (workers.length && workers[0] == myKey) {
      if (role != "generator") {
        log(`Generator ${id}`);
        role = "generator"
        return client.setAsync(myKey, free, "PX", lifeTime)
        .then(() => {
          return client.setAsync("message_count", 0);
        })
      } else {
        return Promise.resolve()        
      }
    } else {
      if (role != "worker") {
        log(`Worker ${id}`);
        role = "worker"
        return client.setAsync(myKey, free, "PX", lifeTime)          
      } else {
        return Promise.resolve()
      }
    }
  })
  .catch((err) => {
    console.error("init@err:", err);
  });
}

var generate = () => {
  var messageCount;
  return client.getAsync('message_count')
  .then((count) => {
    messageCount = Number(count) + 1
    if (messagesMax && messageCount < messagesMax) {
      var message = randomstring.generate(100);
      return client.lpushAsync("messages", message)
      .then(() => {
        return client.incrAsync("message_count")
      })
      .then(() => {
        return client.publishAsync("worker", "new message")    
      })
      .then(() => {
        if (messageCount % 1000 == 0) {
          log(`generate@messageCount: ${messageCount}`);
        }
        log(`generate@message: ${message}`);
        return Promise.resolve();
      })
    } else {
      console.log(`MessagesMax of ${messagesMax} achieved`);
      return Promise.resolve();
    }
  })
  .catch((err) => {
    console.error("generate@err:", err);
  })
}

// var generate = () => {
//   var messageCount;
//   return client.getAsync('message_count')
//   .then((count) => {
//     messageCount = Number(count) + 1
//     if (messagesMax && messageCount < messagesMax) {
//       var message = randomstring.generate(100);
//       return client.lpushAsync("messages", message)
//       .then(() => {
//         return client.incrAsync("message_count")
//       })
//       .then(() => {
//         if (messageCount % 1000 == 0) {
//           log(`generate@messageCount: ${messageCount}`);
//         }
//         log(`generate@message: ${message}`);
//         return Promise.resolve();
//       })
//     } else {
//       console.log(`MessagesMax of ${messagesMax} achieved`);
//       return Promise.resolve();
//     }
//   })
//   .catch((err) => {
//     console.error("generate@err:", err);
//   });
// };

var receive = () => {
  if (free == 1) {
    // Blocking right pop to guarantee only one pop at time
    return client.brpopAsync("messages", maxBlockingTime)
    .then((messageArray) => {
      if (messageArray) {
        free = 0
        var execute;
        var message = messageArray[1]
        log(`receive@message: ${message}`)
        if (Math.random() <= errorProbability && message) {
          execute = client.lpushAsync("error_messages", message);
        } else {
          execute = Promise.resolve();
        }
        return execute
        .delay(messageProcessingTime)
        .then(function() {
          free = 1
          return Promise.resolve();
        });
      } else {
        // log("No message available")
        return Promise.resolve()
      }
    })
    .catch((err) => {
      console.error("receive@err:", err)
    })
  } else {
    return Promise.resolve();
  } 
  
};

client.on("error", (err) => {
  console.error("client@err:", err);
});

client.on('connect', () => {
  if (argv.getErrors === true) {
    client.lrangeAsync("error_messages", 0, -1)
    .then((items) => {
      if (items.length) {
        console.log("Error messages:");
        client.del("error_messages");
        items.reverse().forEach((item, index) => {
          console.log(`№${index}: ${item}`);
        })
      } else {
        console.log("No error messages found")
      }
      process.exit(0);
    })
    .catch((err) => {
      console.error("getErrors@err:", err);
    });
  } else {
    // Start endless loop
    return Promise.resolve()
    .then(function loop() {
      return init().then(function() {
        var execute
        if (role == "generator") {
          execute = generate();
        } else {
          execute = receive();
        }
        return execute
        .delay(loopTime)
        .then(loop);
      });
    })
    .catch((err) => {
      console.error("loop@err:", err);
    });
  }
})

subscriber.on("error", (err) => {
  console.error("subscriber@err:", err);
});

subscriber.on('connect', () => {
  subscriber.subscribe("worker");
})

subscriber.on("message", function(channel, message) {
  if (role == "worker") {
    if (free == 1) {
      // log("Catching new message...")
      return receive()      
    } else {
      // log("I'm busy")
      return Promise.resolve();
    }
  }
});
